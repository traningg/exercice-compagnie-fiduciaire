<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class ClientOperationsController extends Controller
{
    protected $request;
    protected $api;
    protected $operations;
    protected $computed_operations;

    /**
     * ClientOperationsController constructor.
     * @param Request $request Avoid calling Request each time, since the controller always take a Request as a parameter
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
        // Initializes CURL to fetch the API
        try {
            $this->connectToApi();
        } catch (Exception $e) {
            $this->handleErrors($e);
        }
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     * This function is bound to the route /operations and returns all operations for said account
     */
    public function acountOperationsByDate()
    {
        return response()->json(
        // We return, as json, the filtered data
            $this->filterOnRIBAndDate(
                $this->computed_operations,
                $this->request->rib,
                $this->carbonizeDate($this->request->date_min),
                $this->carbonizeDate($this->request->date_max)
            )
        );
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     * This function is bound to the route /solde and returns the balance of all operations for said account
     */
    public function accountBalanceByDate()
    {
        return response()->json([
                // We return, as json, the filtered data
                "RIB" => $this->request->rib,
                "Solde" => $this->getBalanceFromRIBByDate(
                    $this->computed_operations,
                    $this->request->rib,
                    $this->carbonizeDate($this->request->date_min),
                    $this->carbonizeDate($this->request->date_max)
                )]
        );
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     * This function returns all the RIB made available by the API
     */
    public function ribAutocompletion()
    {
        return response()->json($this->computed_operations->map(function ($operation) {
            return collect($operation)->only('RIB');
        })->flatten()->unique()->values
        ());
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     * This function returns the minimum and maximum dates at which operations exist.
     */
    public function datesAutoDisable()
    {
        $first = $this->computed_operations
            ->sortBy('timestamp')
            ->map(function ($operation) {
                return collect($operation)->only('Date');
            })
            ->first()
            ->flatten();

        $last = $this->computed_operations
            ->sortBy('timestamp')
            ->map(function ($operation) {
                return collect($operation)->only('Date');
            })
            ->last()
            ->flatten();

        return response()->json(['first' => $first[0], 'last' => $last[0]]);
    }

    /**
     * @throws Exception
     * This function connects to the API, throws various Exception in case of errors, and sets the results in the class
     * if everything is in order
     */
    protected function connectToApi()
    {
        $ch = curl_init("https://agrcf.lib.id/exercice@dev/");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // Transform the result in a Laravel collection to use its helpers functions
        $result = collect(json_decode(curl_exec($ch), true));

        // If cURL returns an error, throws an error.
        if (curl_errno($ch) || $result->has('error')) {
            throw new Exception('Erreur serveur', 403);
        }

        // If the API itself returns anything else other than 'OK', throws an error.
        if ($result->get('statut') !== 'OK') {
            throw new Exception('Impossible de joindre l\'api', 404);
        }
        return $this->setAPIresults($result);
    }

    /**
     * @param $result
     * This function is only called on a successful API call, sets the results and transforms them.
     */
    protected function setAPIresults($result)
    {
        $this->operations = collect($result['operations']);
        // The results are mutated and set to another property to be easily handled by PHP without types errors.
        $this->computed_operations = $this->transformApiResultAsComputable();
    }

    /**
     * @param \Exception $error
     * @return \Illuminate\Http\JsonResponse
     * This functions is called if errors happen
     */
    protected function handleErrors(\Exception $error)
    {
        return response($error->getMessage(), $error->getCode());
    }

    /**
     * @param Collection $operations A collection of operations
     * @param $rib_requested The RIB requested by the user
     * @param \DateTime $date_min The minimum date to filter on
     * @param \DateTime $date_max The maximum date to filter on
     * @return array
     * This function lists operations, filtered on RIB and Date range.
     * It returns a filtered collection for the API routes.
     */
    protected function filterOnRIBAndDate(Collection $operations, $rib_requested, \DateTime $date_min, \DateTime $date_max)
    {
        return $operations
            ->where('RIB', $rib_requested)
            ->filter(function ($operation) use ($date_max, $date_min) {
                return ($operation['Date']->greaterThanOrEqualTo($date_min) AND $operation['Date']->lessThanOrEqualTo($date_max));
            })
            ->map(function ($operation) {
                $operation['Date'] = Carbon::parse($operation['Date'])->format('d/m/Y');
                return $operation;
            })
            ->sortBy('timestamp')
            ->values()->all();
    }

    /**
     * @param Collection $operations A collection of operations
     * @param $rib_requested The RIB requested by the user
     * @param \DateTime $date_min The minimum date to filter on
     * @param \DateTime $date_max The maximum date to filter on
     * @return array
     * This function sums operations for a given RIB, within a Date range.
     * It returns a filtered collection for the API routes.
     */
    private function getBalanceFromRIBByDate(Collection $operations, $rib_requested, \DateTime $date_min, \DateTime $date_max)
    {
        return $operations
            ->where('RIB', $rib_requested)
            ->filter(function ($operation) use ($date_max, $date_min) {
                return ($operation['Date']->greaterThanOrEqualTo($date_min) AND $operation['Date']->lessThanOrEqualTo($date_max));
            })
            ->sum(function ($operation) {
                return $operation['Montant'];
            });
    }

    /**
     * @return Collection Creates a new collection from the api results
     */
    protected function transformApiResultAsComputable()
    {
        return $this->operations->map(function ($operation) {
            $operation['Date'] = Carbon::createFromFormat('d/m/Y', $operation['Date']);
            $operation['Montant'] = (float)str_ireplace(',', '.', $operation['Montant']);
            if ($operation['Montant'] >= 0) {
                $operation['Recette'] = $operation['Montant'];
                $operation['Dépense'] = 0;
            } else {
                $operation['Dépense'] = -($operation['Montant']);
                $operation['Recette'] = 0;
            }
            $operation['timestamp'] = $operation['Date']->getTimestamp();
            return $operation;
        });
    }

    /**
     * @param $date
     * @return bool|\DateTime
     * This function is responsible of mutating string dates to Carbon objects.
     */
    protected function carbonizeDate($date)
    {
        return Carbon::createFromFormat('d/m/Y', $date);
    }
}
