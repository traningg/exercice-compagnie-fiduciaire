// Import Vue & Components
import Vue from 'vue'
import AirbnbStyleDatepicker from 'vue-airbnb-style-datepicker'
import Home from './components/Home'
import 'vue-airbnb-style-datepicker/dist/vue-airbnb-style-datepicker.min.css'


// Make our components globally available
Vue.use(AirbnbStyleDatepicker);
Vue.component('home', Home);
// Init Vue
new Vue({
    el: '#app',
});
