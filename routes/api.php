<?php

use Illuminate\Http\Request;

Route::get('operations', 'ClientOperationsController@acountOperationsByDate');
Route::get('solde', 'ClientOperationsController@accountBalanceByDate');
Route::get('autocomplete', 'ClientOperationsController@ribAutocompletion');
Route::get('disables-dates', 'ClientOperationsController@datesAutoDisable');
