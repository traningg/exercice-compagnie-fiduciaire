# Prérequis

* PHP >= 7.1.3
* OpenSSL PHP Extension
* PDO PHP Extension
* Mbstring PHP Extension
* Tokenizer PHP Extension
* XML PHP Extension
* Ctype PHP Extension
* JSON PHP Extension
* BCMath PHP Extension
* Composer
* Npm

# Installation

```
git clone https://traningg@bitbucket.org/traningg/exercice-compagnie-fiduciaire.git
cd exercice-compagnie-fiduciaire/
composer install
cp .env.example .env
php artisan key:generate
npm install
npm run prod
php artisan serve
```
Est aussi disponible http://vps591204.ovh.net
